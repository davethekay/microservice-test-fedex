package com.microlearnings.interview.fedex.MicroserviceTest.Model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

// This problem is using a technique I found where they use an embedded ID
// class to hold two fields used for solving the saving of two objects into
// a bridge table using @ManyToMany

@Embeddable
public class StudentCourseId implements Serializable {
	
	private static final long serialVersionUID = -10586919571923493L;

	@Column
	private Long studentIdFk;
	
	@Column
	private Long courseIdFk;
	
	public StudentCourseId() {}
	
	public StudentCourseId(Long studentId, Long courseId) {
		this.studentIdFk = studentId;
		this.courseIdFk = courseId;
	}

	// This is required for embedded complex keys
	
	public Long getStudentIdFk() {
		return studentIdFk;
	}

	public void setStudentIdFk(Long studentIdFk) {
		this.studentIdFk = studentIdFk;
	}

	public Long getCourseIdFk() {
		return courseIdFk;
	}

	public void setCourseIdFk(Long courseIdFk) {
		this.courseIdFk = courseIdFk;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		
		if (o == null || this.getClass() != o.getClass()) return false;
		
		StudentCourseId that = (StudentCourseId) o;	// Cast O to be this embeddable ID class
		return (Objects.equals(studentIdFk, that.studentIdFk) && Objects.equals(courseIdFk, that.courseIdFk));
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(studentIdFk, courseIdFk);
	}

}
