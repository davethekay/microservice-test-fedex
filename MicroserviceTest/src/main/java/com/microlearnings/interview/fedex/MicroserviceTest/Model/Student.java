package com.microlearnings.interview.fedex.MicroserviceTest.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
//import java.sql.Date;
//import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
//import javax.persistence.OneToMany;
//import javax.persistence.ManyToMany;
import javax.persistence.Table;
//import javax.persistence.TableGenerator;
import javax.persistence.Version;

@Entity
@Table
public class Student implements Serializable {

	private static final long serialVersionUID = -9161739105655669353L;

	public Student() {}
	
	public Student(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idStudent;
	
	private String firstName;
	
	private String lastName;
	
	@Version
	private Long version;
	
	@OneToMany(mappedBy="studentIdFk", cascade = CascadeType.ALL, orphanRemoval=true)
	protected List<StudentCourse> courses = new ArrayList<>();
	
	// According to an article, we need to include add and remove methods in order
	// to keep all sides of the association in sync
	
	
	public void addCourse(Course course) {
		// Create a StudentCourse object using this Student object and the passed in
		// Course object. 
		// 
		// NOTE: I have in each of the Student and Course classes a relationship
		// field that is a List of StudentCourse objects, NOT Student or Course objects.
		// I made this mistake and I corrected it in Student and Course classes
		StudentCourse studentCourse = new StudentCourse(this, course);
		// Then we add into the relation field 'courses' the new course we want to add
		courses.add(studentCourse);
		// Finally we add into the Course object the new StudentCourse object as well
		course.getStudents().add(studentCourse);
	}
	
	public Long getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(Long idStudent) {
		this.idStudent = idStudent;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		
		if (o == null || this.getClass() != o.getClass()) return false;
		
		Student that = (Student) o;	// Cast O to be this embedable ID class
		return (Objects.equals(firstName, this.firstName) &&
				Objects.equals(lastName, that.lastName));
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName);
	}
}
