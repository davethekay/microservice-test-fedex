package com.microlearnings.interview.fedex.MicroserviceTest.Services;

import org.springframework.beans.factory.annotation.Autowired;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Student;
import com.microlearnings.interview.fedex.MicroserviceTest.Repositories.StudentRepository;

public class StudentService {

	@Autowired
	StudentRepository studentRepository;
	
	public Student createStudent(String firstName, String lastName) {
		Student student = studentRepository.findByLastNameAndFirstName(lastName, firstName);
		if (student != null) 
			throw new RuntimeException("Student already exists: " + firstName + " " + lastName);
		student = new Student(firstName,lastName);
		return studentRepository.save(student);
	}
}
