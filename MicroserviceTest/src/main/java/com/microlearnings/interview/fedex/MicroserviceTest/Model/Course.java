package com.microlearnings.interview.fedex.MicroserviceTest.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table
public class Course implements Serializable {

	private static final long serialVersionUID = 8222241589744787452L;

	public Course() {}
	
	public Course (String courseName, String courseCredits) {
		this.courseName = courseName;
		this.courseCredits = courseCredits;
	}
	
	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idCourse;
	
	private String courseName;
	
	private String courseCredits;

	@Version
	private Long version;
	
	@OneToMany(mappedBy="courseIdFk", cascade = CascadeType.ALL, orphanRemoval=true)
	protected List<StudentCourse> students = new ArrayList<>();

	public Long getIdCourse() {
		return idCourse;
	}

	public void setIdCourse(Long idCourse) {
		this.idCourse = idCourse;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseCredits() {
		return courseCredits;
	}

	public void setCourseCredits(String courseCredits) {
		this.courseCredits = courseCredits;
	}

	public List<StudentCourse> getStudents() {
		return students;
	}

	public void setStudents(List<StudentCourse> students) {
		this.students = students;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		
		if (o == null || this.getClass() != o.getClass()) return false;
		
		Course that = (Course) o;	// Cast O to be this embedable ID class
		return (Objects.equals(courseName, this.courseName) &&
				Objects.equals(courseCredits, that.courseCredits));
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(courseName, courseCredits);
	}
}
