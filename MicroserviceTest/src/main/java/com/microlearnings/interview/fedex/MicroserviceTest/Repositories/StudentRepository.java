package com.microlearnings.interview.fedex.MicroserviceTest.Repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {
	
	@SuppressWarnings("unchecked")
	public Student save(Student student);
	
	public void deleteById(Long studentId);
	
	public void deleteByLastNameAndFirstName(String lastName, 
											 String firstName);
	
	public Student findByLastNameAndFirstName (String lastname, 
													 String firstname);
	
	//public Optional<Student> findById(Long studentId);

}
