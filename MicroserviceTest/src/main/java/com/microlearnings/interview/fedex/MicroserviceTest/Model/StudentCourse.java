package com.microlearnings.interview.fedex.MicroserviceTest.Model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.StudentCourseId;

@Entity
@Table
public class StudentCourse implements Serializable {

	private static final long serialVersionUID = -3157923323373938070L;
	
	@EmbeddedId
	private StudentCourseId id;

	@ManyToOne(fetch=FetchType.LAZY)
	@MapsId("studentId")
	private Student studentIdFk;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@MapsId("courseId")
	private Course courseIdFk;

	@Version
	private Long version;

	public StudentCourse() {}
	
	public StudentCourse(Student studentIdFk, Course courseIdFk) {
		this.studentIdFk = studentIdFk;
		this.courseIdFk = courseIdFk;
		this.id = new StudentCourseId(studentIdFk.getIdStudent(), courseIdFk.getIdCourse());
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
 
        if (o == null || getClass() != o.getClass())
            return false;
 
        StudentCourse that = (StudentCourse) o;
        return Objects.equals(studentIdFk, that.studentIdFk) &&
               Objects.equals(courseIdFk, that.courseIdFk);
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(studentIdFk, courseIdFk);
    }
    
	public StudentCourseId getId() {
		return id;
	}

	public void setId(StudentCourseId id) {
		this.id = id;
	}

	public Student getStudentIdFk() {
		return studentIdFk;
	}

	public void setStudentIdFk(Student studentIdFk) {
		this.studentIdFk = studentIdFk;
	}

	public Course getCourseIdFk() {
		return courseIdFk;
	}

	public void setCourseIdFk(Course courseIdFk) {
		this.courseIdFk = courseIdFk;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
