package com.microlearnings.interview.fedex.MicroserviceTest.Controllers;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Course;
import com.microlearnings.interview.fedex.MicroserviceTest.Model.Student;
import com.microlearnings.interview.fedex.MicroserviceTest.Model.StudentCourse;

@RestController
@RequestMapping("/register")
public class RegistrationController {

	@PersistenceContext
	EntityManager em;
	
	@RequestMapping(value="/initialize", method=RequestMethod.GET)
	@Transactional
	public void initializeStudentsAndCourses () {
		// We create some students first
		em.persist(new Student("Andrew", "Cohen"));
		em.persist(new Student("David", "Kavanaugh"));
		em.persist(new Student("Johann", "Bach"));
		
		// Now we create some courses second
		em.persist(new Course("Electronics-1", "3"));
		em.persist(new Course("Calculus-1", "4"));
		em.persist(new Course("Drafting-1", "2"));
		em.persist(new Course("Electronics-1", "3"));
		
		em.flush();
		
	}

	// This is the straightforward way I know to match a student to a course
	@RequestMapping(value="/{studentId}/{courseId}", method=RequestMethod.POST)
	@Transactional
	public void registerStudentWithCourse(@PathVariable Long studentId, @PathVariable Long courseId) {
		// Using the ID's we find their objects
		System.out.println("Student ID= " + studentId + ", Course ID=" + courseId);
		Student student = em.find(Student.class, studentId);
		if (student == null) {System.out.println("Student ID was not found"); }
		Course course = em.find(Course.class, courseId);
		if (course == null) {System.out.println("Course ID was not found"); }
		StudentCourse studentCourse = new StudentCourse(student, course);
		em.persist(studentCourse);
	}
	
}