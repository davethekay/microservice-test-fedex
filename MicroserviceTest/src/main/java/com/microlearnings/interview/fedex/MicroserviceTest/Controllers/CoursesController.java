package com.microlearnings.interview.fedex.MicroserviceTest.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Course;
import com.microlearnings.interview.fedex.MicroserviceTest.Repositories.CourseRepository;

@RestController
@RequestMapping(value = "/courses")
public class CoursesController {

	@Autowired
	private CourseRepository CourseRepository;

	// This could also be done using mime type application/JSON but I do not know
	// how at this time. Need to research this.
	@RequestMapping(value="/{courseName}/{courseCredits}", method = RequestMethod.POST)
	public Course save(@PathVariable String courseName, 
					   @PathVariable String courseCredits) {
		Course newCourse = new Course(courseName, courseCredits);
		return CourseRepository.save(newCourse);
	}
	
	// This unfortunately cannot tell if the first path variable is a String or Integer.
	// So I will have to use the ID
	/*
	@RequestMapping(value="/{courseName}", method = RequestMethod.DELETE)
	@Transactional
	public void deleteByCourseName(@PathVariable String courseName) {
		System.out.println("Ready to delete Course: First Name=" + courseName);
		CourseRepository.deleteByCourseName(courseName);
	}
	*/
	
	@RequestMapping(value="/{courseId}", method = RequestMethod.DELETE)
	public void deleteByCourseId (@PathVariable Long courseId) {
		System.out.println("Ready to delete Course: Course ID=" + courseId);
		CourseRepository.deleteById(courseId);
	}
	
	//Cannot resolve ambiguity
	@RequestMapping(value = "/{courseId}", method = RequestMethod.GET)
	public Course findById(@PathVariable Long courseId) {
		Optional<Course> Course = CourseRepository.findById(courseId);
		if (Course.isPresent()) return Course.get(); else return null;
	}

	// Same ambiguity 
	/*
	@RequestMapping(value="/{courseName}", method = RequestMethod.GET)
	public List<Course> findByCourseName (@PathVariable String courseName) {
		System.out.println("Ready to delete Course: First Name=" + courseName);
		List<Course> courses = CourseRepository.findByCourseName(courseName);
		return courses;
	}
	*/
	
}


