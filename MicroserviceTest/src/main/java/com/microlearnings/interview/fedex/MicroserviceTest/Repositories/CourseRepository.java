package com.microlearnings.interview.fedex.MicroserviceTest.Repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Course;

public interface CourseRepository extends CrudRepository<Course, Long> {
	
	@SuppressWarnings("unchecked")
	public Course save(Course course);
	
	public void deleteById(Long courseId);
	
	public Course deleteByCourseName(String courseName); 
	
	public Course findByCourseName (String courseName);

}
