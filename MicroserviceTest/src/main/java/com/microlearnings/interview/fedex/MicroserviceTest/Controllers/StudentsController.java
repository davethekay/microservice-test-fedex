package com.microlearnings.interview.fedex.MicroserviceTest.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Student;
import com.microlearnings.interview.fedex.MicroserviceTest.Repositories.StudentRepository;

@RestController
@RequestMapping(value = "/students")
public class StudentsController {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@RequestMapping(value="/{lastName}/{firstName}", method = RequestMethod.POST)
	public Student save(@PathVariable String lastName, 
						@PathVariable String firstName) {
		Student newStudent = new Student(firstName, lastName);
		return studentRepository.save(newStudent);
	}
	
	@RequestMapping(value="/{lastName}/{firstName}", method = RequestMethod.DELETE)
	@Transactional
	public void deleteByLastnameAndFirstname(@PathVariable String lastName, 
											 @PathVariable String firstName) {
		System.out.println("Ready to delete Student: First Name=" + firstName + ", Last Name=" + lastName);
		studentRepository.deleteByLastNameAndFirstName(lastName, firstName);
	}
	
	@RequestMapping(value="/{studentId}", method = RequestMethod.DELETE)
	public void deleteByStudentId (@PathVariable Long studentId) {
		studentRepository.deleteById(studentId);
	}
	
	@RequestMapping(value = "/{studentId}", method = RequestMethod.GET)
	public Student findById(@PathVariable Long studentId) {
		Optional<Student> student = studentRepository.findById(studentId);
		if (student.isPresent()) return student.get(); else return null;
	}
	
	@RequestMapping(value="/{lastName}/{firstName}", method = RequestMethod.GET)
	public Student findByLastNameAndFirstname (@PathVariable String lastName, 
													 @PathVariable String firstName) {
		System.out.println("Ready to delete Student: First Name=" + firstName + ", Last Name=" + lastName);
		Student student = studentRepository.findByLastNameAndFirstName(lastName, firstName);
		return student;
	}

}
