package com.microlearnings.interview.fedex.MicroserviceTest.Services;

import org.springframework.beans.factory.annotation.Autowired;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Course;
import com.microlearnings.interview.fedex.MicroserviceTest.Model.Student;
import com.microlearnings.interview.fedex.MicroserviceTest.Repositories.CourseRepository;

public class CourseService {
	
	@Autowired
	private CourseRepository courseRepository;
	
	public Course createCourse(String courseName, String courseCredits) {
		Course Course = courseRepository.findByCourseName(courseName);
		if (Course != null) 
			throw new RuntimeException("Course already exists: " + courseName);
		Course = new Course(courseName,courseCredits);
		return courseRepository.save(Course);
	}


}
