package com.microlearnings.interview.fedex.MicroserviceTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.microlearnings.interview.fedex.MicroserviceTest.Model.Student;
import com.microlearnings.interview.fedex.MicroserviceTest.Services.CourseService;
import com.microlearnings.interview.fedex.MicroserviceTest.Services.StudentService;

@SpringBootApplication
@EnableAutoConfiguration
public class MicroserviceTestApplication implements CommandLineRunner {

	@Autowired
	private StudentService studentService;
	
	@Autowired
	private CourseService courseService;

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceTestApplication.class, args);
	}

	// Initialize the DB. BE SURE TO DELETE THE TABLES BEFORE RUNNING
	@Override
	public void run(String... args) throws Exception {
		// Add several records for Students and Courses
		studentService.createStudent("David", "Kavanaugh");
		studentService.createStudent("Andrew", "Cohen");
		studentService.createStudent("Johann", "Bach");
		
		courseService.createCourse("Electronics1", "4");
		courseService.createCourse("Calculus1", "3");
		courseService.createCourse("Philosophy", "2");
		courseService.createCourse("Calculus2", "3");
		courseService.createCourse("Calculus3", "3");
	}

}
