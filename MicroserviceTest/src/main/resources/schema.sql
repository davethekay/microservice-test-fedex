create database microservice_test;
use microservice_test;

create table student (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	firstname CHAR(25),
	lastname CHAR(25),
    version long
);

create table course (
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	coursename char(30),
    coursecredits char(4),
    version long
);

create table student_course (
	studentId INT NOT NULL PRIMARY KEY,
	courseId INT NOT NULL PRIMARY KEY,
	
)